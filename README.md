# excle-to-img

excle-to-img介绍
在自动化办公开发中的一个小任务，将excle某个sheet里的内容转化为图片。

#### 软件架构
软件架构说明


#### 需要用到的库

1.  pip install xlwings
2.  pip install pypiwin32(由于依赖问题，版本不要最新的版本，否则会触发importError:DLL的问题.建议版本为220)
3.  pip install  pillow

#### 使用说明

1.  修改excle2img中的函数即可，修改你的文件的路径，或者直接直接调用函数

#### 参与贡献

Jerry_ym

2020.08.24
增加说明，这个方法并不是很稳定，有时候会出现删除图片不成功的情况导致下次再截图的时候容易报错，建议多try几次删除excle中实时图片。再进行下一步。